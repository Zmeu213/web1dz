from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

from cgi import parse_qs

def application(environ, start_response):
	string = []
	string.append('Hello world')

	string.append(environ['REQUEST_METHOD'])
	get_params = parse_qs(environ['QUERY_STRING'])
	for key, value in get_params.items():
		string.append("{0}: {1}".format(key, value))

	string.append('POST')
	post_params = parse_qs(environ['wsgi.input'].read())
	for key, value in post_params.items():
		string.append("{0}: {1}".format(key, value))

	start_response("200 OK", [("Content-Type", "text/plain")])
	return ["\n".join(string)]

def helloworld(request):
    string = []
    string.append('Hello world!\n')

    return HttpResponse(string)