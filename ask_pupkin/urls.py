from django.conf.urls import patterns, include, url
from django.contrib import admin

from ask_pupkin import views


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ask_pupkin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns = patterns('',
    url(r'^$', views.helloworld, name='index'),
)